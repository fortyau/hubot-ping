# Description:
#   Ping a server
#
# Commands:
#   hubot ping host - Display all users that hubot knows about

module.exports = (robot) ->
  robot.respond /ping (.*)$/i, (msg) ->
    host = msg.match[1]
    @exec = require('child_process').exec
    command = "ping -c 1 #{host}"
    @exec command, (stdout) -> 
      msg.send stdout


