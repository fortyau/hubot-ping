### Install
Put this in your Hubot/scripts directory, restart hubot.
  
  
### Usage
 
*Type:*  
    hubot ping google.com
  
*Receive:*  
    "PING google.com (74.125.21.139) 56(84) bytes of data.
    64 bytes from yv-in-f139.1e100.net (74.125.21.139): icmp_seq=1 ttl=44 time=19.8 ms"
    --- google.com ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 40ms
    rtt min/avg/max/mdev = 19.800/19.800/19.800/0.000 ms
